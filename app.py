import re
import logging
from PIL import Image
from pylibdmtx.pylibdmtx import encode
from pylibdmtx.pylibdmtx_error import PyLibDMTXError
from pathlib import Path
from flask import Flask, render_template, request, send_file


def set_filename(filename):
    """
    Strips/sanitizes the passed in string, for filename.
    :param filename:
    :return: sanitized filename
    """
    filename = re.sub(r'\W', '_', filename)
    return filename


# Set the logging format/information
logging.basicConfig(level=logging.INFO, format="BC9K:%(levelname)s:%(message)s")

# Set the output path for the generated images
outpath = Path(Path.cwd(), 'static')
if not outpath.exists():
    try:
        Path.mkdir(outpath)
    except (PermissionError, OSError) as err:
        raise err


# Create the Flask application context
bc9k = Flask(__name__)


@bc9k.route('/')
def index():
    """
    The main index page.
    :return: index
    """
    return render_template('index.html.j2')


@bc9k.route('/generate')
def genbarcode():
    """
    The barcode generation endpoint.
    The data to be encoded is passed as a URL parameter, 'data'
    E.g. '/generate?data=ABCD1234'
    Size can be set according to pylibdmtx values:
    E.g. '/generate?data=ABCD1234&size=32x32'

    If the image already exists in the output directory, it will
    be served instead of invoking the generator.
    :return: image file
    """
    # Render general template if no data passed in.
    if len(request.args) == 0 or request.args.get('data') is None:
        return render_template('generate.html.j2')
    else:
        data = request.args.get('data')
        # Set the size
        if request.args.get('size') is None:
            dmsize = "SquareAuto"
        else:
            dmsize = request.args.get('size')

        # Sanitize the filename and set the output pathname.
        filename = set_filename(data)

        # Check the arguments to generate the output filename and path
        if request.args.get('scale'):
            scale = int(request.args.get('scale'))
            outsize = "s{}".format(str(scale))
        elif request.args.get('width') and request.args.get('height'):
            out_width = int(request.args.get('width'))
            out_height = int(request.args.get('height'))
            outsize = "{}x{}".format(str(out_width), str(out_height))
        else:
            outsize = "noscale"

        # Set the image path
        image_path = Path(outpath, "{}-{}_{}.png".format(filename, dmsize, outsize))

        if image_path.exists():
            # If the barcode already exists, skip generation.
            logging.info("Barcode {}-{}_{} already exists, serving cached file.".format(filename, dmsize, outsize))
            pass
        else:
            try:
                # Invoke the datamatrix encoder
                # In the event of an error, catch and return error image.
                logging.info("Generating barcode: '{}','{}','{}'".format(data, dmsize, outsize))
                dm = encode(data.encode('utf-8'), size=dmsize)

                # Pass to PIL
                img = Image.frombytes('RGB', (dm.width, dm.height), dm.pixels)

                # Resize, if needed
                if 'scale' in locals():
                    out_width = dm.width * scale
                    out_height = dm.height * scale
                    img = img.resize((out_width, out_height), Image.HAMMING)
                elif 'out_width' in locals() and 'out_height' in locals():
                    img = img.resize((out_width, out_height), Image.HAMMING)

                img.save(str(image_path), format="PNG")
            except (PyLibDMTXError, IOError, KeyError) as imgerr:
                logging.error("Generation error: {}".format(imgerr))
                return send_file(str(Path(outpath, 'app', 'generate_error.png')))
        return send_file(str(image_path))


if __name__ == '__main__':
    bc9k.run()
