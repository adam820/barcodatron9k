FROM fedora
RUN dnf install libdmtx pip cronie zlib-devel libjpeg-turbo-devel gcc python3-devel -y && dnf clean all
COPY . /barcodatron9k
WORKDIR /barcodatron9k
RUN echo -e '#!/bin/sh\nrm -f /barcodatron9k/static/*.png' > /etc/cron.weekly/barcoderemovercron.sh
RUN chmod +x /etc/cron.weekly/barcoderemovercron.sh
RUN pip3 install -r requirements.txt
EXPOSE 8080
CMD gunicorn -w4 -b 0.0.0.0:8080 app:bc9k
